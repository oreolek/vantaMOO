# -*- coding: utf-8 -*-
"""
Connection screen

This is the text to show the user when they first connect to the game (before
they log in).

To change the login screen in this module, do one of the following:

- Define a function `connection_screen()`, taking no arguments. This will be
  called first and must return the full string to act as the connection screen.
  This can be used to produce more dynamic screens.
- Alternatively, define a string variable in the outermost scope of this module
  with the connection string that should be displayed. If more than one such
  variable is given, Evennia will pick one of them at random.

The commands available to the user when the connection screen is shown
are defined in evennia.default_cmds.UnloggedinCmdSet. The parsing and display
of the screen is done by the unlogged-in "look" command.

"""

from django.conf import settings

from evennia import utils

CONNECTION_SCREEN = """
|n                                                             |wansi (c) garu|n      |n
|n         |wWelcome to..|n                                                           |n
|n                                                                                |n
|n                                                           |c████   ███|n     |c█|n     |n
|n        |m▀█▀   ▀█▀ ▄█████▄ ▀██▀   █ ▀█████████▀ █████    |C▓▓▓|m████|C▓▓▓|m███|C▓▓▓▓▓|m█|n     |n
|n         |m█     █  █░░░░░█  █░█   █  ░░░░█░░░░ █░░░░░█   ▓▓▓|n|w████|M▓▓▓|n|w███|M▓▓▓▓▓|n|w█|n     |n
|n         |w|[m░|n     |w|[m░|n  |w|[m░|n     |w|[m░|n  |w|[m░|n |w|[m░|n   |w|[m░|n      |w|[m░|n     |w|[m░|n     |w|[m░|n   |w▓▓▓|m████|n|w▓▓▓|m███|n|w▓▓▓▓▓|M█|n     |n
|n         |M░|m|[w▒|n   |m|[w▒|n░  |M|[w░░░░░░░|n  |m|[w▒|n |M░|[w▒|n  |m|[w▒|n      |m|[w▒|n     |m|[w▒▒▒▒▒▒▒|n   |m▓▓▓|C████|M▓▓▓|C███|M▓▓▓▓▓|C█|n     |n
|n          |M░|w█ █|M░   |w█|M░░░░░|w█  █  |M░|w█ █|n      |w█|n     |w█|M░░░░░|w█   |C▓▓▓    ▓▓▓   ▓▓▓▓▓|n      |n
|n           |x|[W░|n|w█|x|[W░|n    |w█|n     |w█  █    ██|n      |w█|n     |w█|n     |w█|n                           |n
|n            |x|[W░|n     |x|[W░|n     |x|[W░|n  |x|[W░|n    |x|[W░░|n      |x|[W░|n     |x|[W░|n     |x|[W░|n                           |n
|n                                         |m▀█▀    ▀█▀  █████    █████▓▓▒▒▒▒░░░░░|n  |n
|n         |wBROUGHT TO YOU BY|n                |m██    ██  █|n|m░░░░░█  █░░░░░█▓▒▒▒░░░░|n    |n
|n                       |mVANTABLACK|n         |m█░█  █░█  █|n     |m█  █|n     |m█▓▒▒░░░|n      |n
|n                                          |m|[w▒|n |M░|w|[m▒▒|n|M░|n |m|[w▒|n  |m|[w▒|n     |m|[w▒|n  |m|[w▒|n     |m|[w▒|n|m▒▒░░░|n       |n
|n                                          |w█  |M░░  |w█  █|n     |w█  █|n     |w█|n            |n
|n                                          |w█|n      |w█  |x|[W░|w█████|x|[W░|n  |x|[W░|w█████|x|[W░|n            |n
|n                                          |x|[W░|n      |x|[W░|n   |x|[W░░░░░█|n   |x|[W░░░░░█|n            |n
|n|x  |rNew users: |ncreate <user> <password>|n                                           |n
|n                                                                                |n
|n|w  |GExisting users: |nconnect <user> <password>|n                                     |n
|n                                                                                |n
|n                                                                                |n

 If you have spaces in your username, enclose it in quotes.
 Enter |whelp|n for more info. |wlook|n will re-show this screen.
""".format(
    settings.SERVERNAME, utils.get_evennia_version("short")
)
